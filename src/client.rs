use std::*;

#[derive(Deserialize, Debug)]
pub struct ChannelszResponse {
    pub cluster_id: String,
    pub server_id: String,
    pub now: String,
    pub offset: u64,
    pub limit: u64,
    pub count: u64,
    pub total: u64,
    pub channels: Option<Vec<ChannelszItem>>,
}

#[derive(Deserialize, Debug)]
pub struct ChannelszItem {
    pub name: String,
    pub subscriptions: Vec<SubszResponse>,
}

#[derive(Deserialize, Debug)]
pub struct SubszResponse {
    pub client_id: String,
    pub is_stalled: bool,
}
