#[macro_use] extern crate serde_derive;
#[macro_use] extern crate quick_error;
extern crate reqwest;
extern crate futures;
extern crate tokio;
use tokio::timer::{Delay};
use tokio::prelude::*;
use std::time::{Instant,Duration};
use reqwest::Client;
//use futures::future::ok;
use futures::Future;
use std::sync::mpsc::channel;
use std::thread;

pub mod client;

quick_error! {
    #[derive(Debug)]
    pub enum MyError {
        Tim(err: tokio::timer::Error) {
            from()
        }
        Req(err: reqwest::Error) {
            from()
        }
    }
}

fn main() {
    let (tx, rx) = channel();
    thread::spawn(move || {
        let cl = Client::new();
        let url = "http://localhost:8222/streaming/channelsz?subs=1";
        loop {
            match cl.get(url).send().and_then(|mut r| {
                r.json::<client::ChannelszResponse>()
            }) {
                Ok(cr) => {
                    if let Err(e) = tx.send(cr) {
                        panic!("error: {:?}", e);
                    }
                    std::thread::sleep(Duration::from_secs(5));
                }
                Err(e) => {
                    println!("error: {:?}", e);
                    std::thread::sleep(Duration::from_secs(1));
                }
            }
        }
    });
    rx.iter().for_each(|r| println!("res: {:?}", r));
}
